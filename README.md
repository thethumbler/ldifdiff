# ldifdiff [![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](http://www.gnu.org/licenses/lgpl-3.0)

An ldifdiff utility, similar to [perl-ldap/ldifdiff](https://metacpan.org/pod/distribution/perl-ldap/contrib/ldifdiff.pl), with focus on performance and resources utilization.

**NOTE**: The tool uses a set of performance trade-offs over validitiy, make sure to read the points below for more infromation. **Use at your own risk.**

## Usage of the ldifdiff command
```
$ ./ldifdiff -h
Usage: ldifdiff <base> <target>
	Generates LDIF change diff between two LDIF files.
```

* `base` is the file you would apply the resulting LDIF changes to.
* `target` is the file to read updates from.

More clearly, `base` is the old dump, `target` is the new dump.

[![asciicast](https://asciinema.org/a/232236.png)](https://asciinema.org/a/232236)

## Performance considerations
In order to achieve high-performance constrains, the tool makes the following assumptions:
* **Input files are conforming to [RFC2849](https://tools.ietf.org/html/rfc2849), no check is done to ensure the conformance.**
* Sufficient memory exists in order to memory map both files in memory (using `mmap` with `PROT_READ` and `MAP_SHARED|MAP_POPULATE`) to reduce the number of syscalls needed.
* Some sane large buffer sizes are pre-configured in `config.h`, only the buffer allocated for line reading grows dynamically with `MAX_LINE` as the base allocation (grows 2x everytime). Ensure the following values:
    - `MAX_ENTRIES`: maximum number of entries in either of the files (default: `50000000`)
    - `MAX_ATTRS`: maximum number of attributes in a single entry, as a flat list. (default: `10000`)
    - `BUF_SIZE`: size of commit buffer for modified entries, must be large enough to fit any modified entry. (default: `2 MB`)

## Building
We use makefile to build the tool, `gcc` and `glibc` are the only dependencies
```
$ make
```

The resulting binary is `./ldifdiff`
