#ifndef _CONFIG_H
#define _CONFIG_H

#define MAX_ENTRIES (50000000UL)
#define MAX_ATTRS   (10000UL)
#define BUF_SIZE    (8192 * 256)
#define MAX_LINE    (2048)

#define XATTRS      {"createTimestamp", "modifyTimestamp"}
#define CISCMP      {"cn", "mail", "manager", "member", "o", "ou", "objectClass", "owner", "uid", "uniqueMember"}

#endif /* _CONFIG_H */
