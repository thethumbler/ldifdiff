CFLAGS += -std=gnu99 -I./ -Iinclude/ -O3 -mcmodel=large -Wall -Wextra -Werror 

obj-y += src/attr.o
obj-y += src/diff.o
obj-y += src/entry.o
obj-y += src/index.o
obj-y += src/ldif.o

ldifdiff: $(obj-y)
	$(CC) $(CFLAGS) -o $@ $(obj-y)

.PHONY: clean
clean:
	$(RM) $(obj-y) ldifdiff
