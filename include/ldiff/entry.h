#ifndef _LDIFF_ENTRY_H
#define _LDIFF_ENTRY_H

#include <ldiff/ldif.h>

void ldif_entry_add(struct ldif_entry *ent);
int  ldif_entry_modify(struct ldif_entry *src, struct ldif_entry *dst);

#endif /* ! _LDIFF_ENTRY_H */
