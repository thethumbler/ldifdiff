#ifndef _LDIFF_LDIF_H
#define _LDIFF_LDIF_H

#include <config.h>

struct rbuf {
    char *base;
    size_t len;
    size_t size;
};

struct ldif {
    int fd;
    size_t size;
    const char *map;
    struct ldif_index *idx;
};

struct ldif_attr {
    const char *key;
    char *value;
    size_t len;
    //int enc;
};

struct ldif_attr_value {
    const char *ptr;
    size_t len;
};

struct ldif_entry_attrs {
    const char *key;
    size_t nr;
    struct ldif_attr_value *values;
};

struct ldif_entry {
    const char *dn;
    size_t nr;
    struct ldif_entry_attrs *attrs;
};

int ldif_open(const char *path, struct ldif *ldif);
int ldif_close(struct ldif *ldif);

int  ldif_line_read(struct ldif *ldif, const char **next, struct rbuf *buf);
int  ldif_line_parse(struct ldif *ldif, const char **next, struct ldif_attr *attr);
int  ldif_entry_parse(struct ldif *ldif, const char **next, struct ldif_entry *ent);
void ldif_entry_free(struct ldif_entry *ent);


int ldif_attrs_cmp(struct ldif_entry_attrs *src, struct ldif_entry_attrs *dst,
    int (*compar)(const char *, const char *));

#endif /* _LDIFF_LDIF_H */
