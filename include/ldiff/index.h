#ifndef _LDIFF_INDEX_H
#define _LDIFF_INDEX_H

#include <config.h>
#include <sys/types.h>
#include <ldiff/diff.h>
#include <ldiff/ldif.h>

struct ldif_index_entry {
    int matched;
    const char *dn;
    const char *ptr;
};

struct ldif_index {
    size_t nr;
    struct ldif_index_entry ent[MAX_ENTRIES];
};

int ldif_index_build(struct ldif *ldif, struct ldif_index *idx);
struct ldif_index_entry *ldif_index_lookup(struct ldif_index *idx, const char *dn);
void ldif_index_dump(struct ldif_index *index);
void ldif_index_free(struct ldif_index *index);

#endif /* ! _LDIFF_INDEX_H */
