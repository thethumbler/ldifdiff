#ifndef _LDIF_DIFF_H
#define _LDIF_DIFF_H

#include <config.h>
#include <stdint.h>

#define _log(...) fprintf(stderr, __VA_ARGS__)

struct ldiff_entry {
    char *p;
    int matched;
};

struct ldiff_attr_flat {
    char *name;
    char *value;
};

struct ldiff_attr {
    char *name;
    int  count;
    char **values;
};

size_t __fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int __commit(char *entry, size_t sz); /* XXX */

/* src/attr.c */
int ldiff_attrs_read(char *entry, struct ldiff_attr *attrs, size_t *count);

/* src/entry.c */
struct ldiff_entry *ldiff_entry_find(char *entry, struct ldiff_entry *entries, uint64_t nr_entries);
int ldiff_entry_modify(char *src, char *dst);
void ldiff_entry_delete(char *entry);
void ldiff_entry_add(char *entry);

static inline int xattr(const char *s)
{
    const char *attrs[] = XATTRS;

    for (size_t i = 0; i < sizeof(attrs)/sizeof(attrs[0]); ++i)
        if (!strcasecmp(s, attrs[i]))
            return 1;

    return 0;
}

static inline int ciscmp(const char *s)
{
    const char *attrs[] = CISCMP;

    for (size_t i = 0; i < sizeof(attrs)/sizeof(attrs[0]); ++i)
        if (!strcasecmp(s, attrs[i]))
            return 1;

    return 0;
}

#endif /* ! _LDIF_DIFF_H */
