#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <ldiff/diff.h>
#include <ldiff/ldif.h>

int ldif_attrs_cmp(struct ldif_entry_attrs *src, struct ldif_entry_attrs *dst,
    int (*compar)(const char *, const char *))
{

    /* if count mismatches, the attributes are different */
    if (src->nr != dst->nr)
        return 1;

    /* else, compare respective values */
    for (size_t i = 0; i < src->nr; ++i) {
        int rc;
        if ((rc = compar(src->values[i].ptr, dst->values[i].ptr)))
            return rc;
    }

    return 0;
}
