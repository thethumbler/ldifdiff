#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>

#include <ldiff/diff.h>
#include <ldiff/ldif.h>
#include <ldiff/entry.h>

void ldif_entry_add(struct ldif_entry *ent)
{
    char *buf = malloc(BUF_SIZE);
    char *p = buf;

    p += sprintf(p, "dn:%s\nchangetype: add\n", ent->dn);

    for (size_t i = 0; i < ent->nr; ++i) {
        for (size_t j = 0; j < ent->attrs[i].nr; ++j) {
            p += sprintf(p, "%s:%s\n", ent->attrs[i].key, ent->attrs[i].values[j].ptr);
        }
    }

    p += sprintf(p, "\n");
    __fwrite(buf, p - buf, 1, stdout);
}

int ldif_entry_modify(struct ldif_entry *src, struct ldif_entry *dst)
{
    char *buf = malloc(BUF_SIZE);
    char *p = buf;

    /* write dn to temporary buffer */
    p += sprintf(p, "dn:%s\n", src->dn);
    p += sprintf(p, "changetype: modify\n");

    int modified = 0;

    size_t i = 0, j = 0;
    while (i < src->nr || j < dst->nr) {
        if (i < src->nr && xattr(src->attrs[i].key)) {
            ++i; continue;
        }

        if (j < dst->nr && xattr(dst->attrs[j].key)) {
            ++j; continue;
        }

        if (i == src->nr) {
            /* attribute not found in source but found in destination -- add */

            /* add seperator if modified */
            if (modified) {
                *p++ = '-';
                *p++ = '\n';
            }

            /* mark entry as modified */
            modified = 1;

            p += sprintf(p, "add: %s\n", dst->attrs[j].key);

            for (size_t k = 0; k < dst->attrs[j].nr; ++k) {
                p += sprintf(p, "%s:%s\n", dst->attrs[j].key, dst->attrs[j].values[k].ptr);
            }

            ++j;
        } else if (j == dst->nr) {
            /* attribute found in source but not found in destination -- delete */

            /* add seperator if modified */
            if (modified) {
                *p++ = '-';
                *p++ = '\n';
            }

            /* mark entry as modified */
            modified = 1;

            p += sprintf(p, "delete: %s\n", src->attrs[i].key);
            ++i;
        } else {
            int cmp = strcasecmp(src->attrs[i].key, dst->attrs[j].key);

            if (cmp < 0) {
                /* source attribute is at a lower lexiographical order,
                 * that is, found in source and not found in target -- delete */

                /* add seperator if modified */
                if (modified) {
                    *p++ = '-';
                    *p++ = '\n';
                }

                /* mark entry as modified */
                modified = 1;

                p += sprintf(p, "delete: %s\n", src->attrs[i].key);
                ++i;
            } else if (cmp > 0) {
                /* source attribute is at a higher lexiographical order,
                 * that is, found in target and not found in source -- add */

                /* add seperator if modified */
                if (modified) {
                    *p++ = '-';
                    *p++ = '\n';
                }

                /* mark entry as modified */
                modified = 1;

                p += sprintf(p, "replace: %s\n", dst->attrs[j].key);

                assert(dst->attrs[j].nr);

                for (size_t k = 0; k < dst->attrs[j].nr; ++k) {
                    p += sprintf(p, "%s:%s\n", dst->attrs[j].key, dst->attrs[j].values[k].ptr);
                }

                ++j;
            } else {
                /* source and target attributes are the same -- compare values */
                if (ldif_attrs_cmp(&src->attrs[i], &dst->attrs[j],
                        ciscmp(src->attrs[i].key) ? strcasecmp : strcmp)) {
                    //_DEBUG("comp attr: %s\n", src_attrs[i].name);

                    /* add seperator if modified */
                    if (modified) {
                        *p++ = '-';
                        *p++ = '\n';
                    }

                    /* mark entry as modified */
                    modified = 1;

                    p += sprintf(p, "replace: %s\n", src->attrs[i].key);

                    //assert(dst->attrs[j].nr);

                    for (size_t k = 0; k < dst->attrs[j].nr; ++k) {
                        p += sprintf(p, "%s:%s\n", dst->attrs[j].key, dst->attrs[j].values[k].ptr);
                    }
                }

                ++i; ++j;
            }
        }
    }

    if (modified) {
        /* commit temporary buffer to output file */
        __commit(buf, p - buf);
    }

    free(buf);

    return modified;
}
