#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>

#include <ldiff/diff.h>
#include <ldiff/ldif.h>

#define CUR (*(*next))
#define INC (++(*next))

int ldif_line_read(struct ldif *ldif, const char **next, struct rbuf *buf)
{
    assert(next);
    assert(buf);

    if (!buf->base) {
        buf->base = malloc(MAX_LINE);
        assert(buf->base);
        buf->size = MAX_LINE;
        buf->len  = 0;
    }

    off_t off = 0;

    while ((*next) < ldif->map + ldif->size) {
        if (CUR == '\n') {
            INC;
            if (CUR == ' ') {INC; continue;}

            buf->base[off] = 0;
            buf->len = off;

            if (buf->base[0] == '#')
                return ldif_line_read(ldif, next, buf);

            return 0;
        }

        if ((size_t) off >= buf->size) {
            buf->size *= 2;
            buf->base = realloc(buf->base, buf->size);
            assert(buf->base);
        }

        buf->base[off++] = CUR; INC;
    }

    return -1;
}

int ldif_line_parse(struct ldif *ldif, const char **next, struct ldif_attr *attr)
{
    assert(ldif);
    assert(next);
    assert(attr);

    struct rbuf buf;
    memset(&buf, 0, sizeof(buf));

    ldif_line_read(ldif, next, &buf);

    assert(buf.base);

    if (!buf.base[0]) {
        free(buf.base);
        buf.base = NULL;
        return -1;
    }

    char *sep = strchr(buf.base, ':');
    *sep = 0; ++sep;

    attr->key = strdup(buf.base);
    assert(attr->key);

    attr->value = strdup(sep);
    assert(attr->value);
    attr->len = buf.len - (sep - buf.base);
    free(buf.base);
    buf.base = NULL;
    return 0;
}

static int attr_compar(const void *_a, const void *_b)
{
    const struct ldif_attr *a = _a;
    const struct ldif_attr *b = _b;
    int rc = strcasecmp(a->key, b->key);
    if (rc) return rc;

    return ciscmp(a->key)?
        strcasecmp(a->value, b->value) :
        strcmp(a->value, b->value);
}

int ldif_entry_parse(struct ldif *ldif, const char **next, struct ldif_entry *ent)
{
    /* build flat list of attributes */
    struct ldif_attr *attrs = malloc(MAX_ATTRS * sizeof(struct ldif_attr));

    size_t nr = 0;
    while (nr < MAX_ATTRS && ldif_line_parse(ldif, next, &attrs[nr]) != -1) {
        ++nr;
    }

    if (nr >= MAX_ATTRS) {
        _log("entry has more than MAX_ATTRS attributes\n");
        exit(-1);
    }

    /* sort attributes */
    qsort(attrs, nr, sizeof(struct ldif_attr), attr_compar);

    /* count unique attribute keys */
    size_t nr_uniq = 0;
    const char *last_key = NULL;
    for (size_t i = 0; i < nr; ++i) {
        if (last_key && !strcasecmp(last_key, attrs[i].key)) {
            continue;
        }

        last_key = attrs[i].key;
        nr_uniq++;
    }

    /* process attributes into a collection of unique attributes */
    struct ldif_entry_attrs *u_attrs = malloc(nr_uniq * sizeof(struct ldif_entry_attrs));
    u_attrs->nr = nr_uniq;

    size_t idx = 0;
    size_t u_idx = 0;
    while (idx < nr) {
        u_attrs[u_idx].key = strdup(attrs[idx].key);

        /* find all attributes with matching names */
        size_t j = idx + 1, count = 1;
        while (j < nr && !strcasecmp(attrs[j].key, attrs[idx].key)) {
            ++j;
            ++count;
        }

        struct ldif_attr_value *values = malloc(count * sizeof(struct ldif_attr_value));

        for (size_t k = 0; k < count; ++k) {
            /* We no longer need the key */
            free((void *) attrs[idx + k].key);

            values[k].ptr = attrs[idx + k].value;
            values[k].len = attrs[idx + k].len;
        }

        /* attributes are already sorted */
        u_attrs[u_idx].values = values;
        u_attrs[u_idx].nr     = count;
        ++u_idx;

        idx += count;
    }

    /* destroy flat attributes */
    free(attrs);

    ent->nr    = nr_uniq;
    ent->attrs = u_attrs;

    return 0;
}

void ldif_entry_free(struct ldif_entry *ent)
{
    for (size_t i = 0; i < ent->nr; ++i) {
        for (size_t j = 0; j < ent->attrs[i].nr; ++j) {
            free((void *) ent->attrs[i].values[j].ptr);
        }

        free((void *) ent->attrs[i].key);
        free((void *) ent->attrs[i].values);
    }

    free(ent->attrs);
}

void ldif_ent_dump(struct ldif_entry *ent)
{
    fprintf(stderr, "-------------------------ENTRY-------------------------\n");
    fprintf(stderr, "\e[36mdn: \e[91m%s\e[0m\n", ent->dn);
    for (size_t i = 0; i < ent->nr; ++i) {
        fprintf(stderr, "\e[36m%s:\e[0m\n", ent->attrs[i].key);
        for (size_t j = 0; j < ent->attrs[i].nr; ++j) {
            fprintf(stderr, "- \e[92m%s\e[0m\n", ent->attrs[i].values[j].ptr);
        }
    }
}

int ldif_open(const char *path, struct ldif *ldif)
{
    int fd = -1;
    struct stat statbuf = {0};
    char *map = NULL;

    if (!path || !ldif) {
        errno = EINVAL;
        goto error;
    }

    if ((fd = open(path, O_RDONLY)) < 0) {
        goto error;
    }

    if (fstat(fd, &statbuf) < 0) {
        goto error;
    }

    map = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED|MAP_POPULATE, fd, 0);

    if (map == MAP_FAILED) {
        goto error;
    }

    ldif->fd   = fd;
    ldif->size = statbuf.st_size;
    ldif->map  = map;
    ldif->idx  = NULL;

    struct ldif_attr attr = {0};
    const char *next = map;

    ldif_line_parse(ldif, &next, &attr);

    if (strcmp(attr.key, "version")) {
        fprintf(stderr, "LDIF file does not start with \"version\"\n");
        exit(-1);
    }

    int version;
    sscanf(attr.value, "%d", &version);

    if (version != 1) {
        fprintf(stderr, "Unsupported LDIF version: %d\n", version);
        exit(-1);
    }

    free((void *) attr.key);
    free((void *) attr.value);

    return 0;

error:
    if (fd > 0)
        close(fd);

    return -1;
}

int ldif_close(struct ldif *ldif)
{
    if (!ldif || !ldif->map) {
        errno = EINVAL;
        goto error;
    }

    if (munmap((void *) ldif->map, ldif->size) == -1)
        goto error;

    if (ldif->fd > 0)
        close(ldif->fd);

    return 0;

error:
    return -1;
}
