#define _GNU_SOURCE

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <errno.h>

#include <ldiff/diff.h>
#include <ldiff/index.h>
#include <ldiff/ldif.h>
#include <ldiff/entry.h>

void usage()
{
    fprintf(stderr, "%s\n",
            "Usage: ldifdiff <base> <target>\n"
            "\tGenerates LDIF change diff between two LDIF files.");
}

int ldiff_ldif_mem_read(char *map, size_t map_size, struct ldiff_entry *ents, uint64_t *count)
{
    uint64_t _count = 0;

    /* find "dn:" */
    for (uint64_t i = 0; i < map_size - 4; ++i) {
        if (!strncmp(&map[i], "\ndn:", 4)) {
            ents[_count++].p = &map[i] + 1;
        }
    }

    if (count)
        *count = _count;

    return 0;
}

size_t __fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    static int lnl = 0;
    const char *_ptr = ptr;
    size_t rc = 0;

    for (size_t i = 0; i < nmemb; ++i) {
        for (size_t j = 0; j < size; ++j) {
            ++lnl;

            if (*_ptr == '\n') {
                lnl = 0;
            } else if (lnl >= 79) {
                lnl = 0;
                rc += fwrite("\n ", 2, 1, stream);
            }

            rc += fwrite(_ptr, 1, 1, stream);
            ++_ptr;
        }
    }

    return rc;
}

int __commit(char *entry, size_t sz)
{
    /* TODO: add output lock */
    __fwrite(entry, sz, 1, stdout);
    __fwrite("\n", 1, 1, stdout);
    return 0;
}

void entry_add(struct ldif_entry *ent)
{
    fprintf(stdout, "dn: %s\nchangetype: add\n", ent->dn);

    for (size_t i = 0; i < ent->nr; ++i) {
        for (size_t j = 0; j < ent->attrs[i].nr; ++j) {
            fprintf(stdout, "%s: %s\n", ent->attrs[i].key, ent->attrs[i].values[j].ptr);
        }
    }

    fprintf(stdout, "\n");
}

int diff(const char *base_path, const char *target_path)
{
    struct ldif base, target;

    _log("Opening file: %s... ", base_path);
    if (ldif_open(base_path, &base) == -1) {
        fprintf(stderr, "ldif_open: %s: %s", base_path, strerror(errno));
        exit(-1);
    }
    _log("%lu bytes\n", base.size);

    _log("Opening file: %s... ", target_path);
    if (ldif_open(target_path, &target) == -1) {
        fprintf(stderr, "ldif_open: %s: %s", target_path, strerror(errno));
        exit(-1);
    }
    _log("%lu bytes\n", target.size);

    _log("Building index of %s...\n", base_path);
    base.idx = calloc(1, sizeof(struct ldif_index));
    ldif_index_build(&base, base.idx);

    _log("Building index of %s...\n", target_path);
    target.idx = calloc(1, sizeof(struct ldif_index));
    ldif_index_build(&target, target.idx);

    size_t added_cnt = 0, modified_cnt = 0, deleted_cnt = 0;

    struct ldif_index_entry *base_idx_ent, *target_idx_ent;
    for (size_t i = 0; i < base.idx->nr; ++i) {
        base_idx_ent = &base.idx->ent[i];
        target_idx_ent = ldif_index_lookup(target.idx, base_idx_ent->dn);

        if (target_idx_ent) {
            /* entry found -- modify */
            target_idx_ent->matched = 1;
            struct ldif_entry base_ent, target_ent;
            const char *s_next = base_idx_ent->ptr;
            const char *d_next = target_idx_ent->ptr;

            base_ent.dn = base_idx_ent->dn;
            target_ent.dn = target_idx_ent->dn;

            ldif_entry_parse(&base, &s_next, &base_ent);
            ldif_entry_parse(&target, &d_next, &target_ent);

            if (ldif_entry_modify(&base_ent, &target_ent))
                ++modified_cnt;

            ldif_entry_free(&base_ent);
            ldif_entry_free(&target_ent);
        } else {
            /* entry not found -- delete */
            ++deleted_cnt;
            fprintf(stdout, "dn:%s\nchangetype: delete\n\n", base_idx_ent->dn);
        }
    }

    /* add all unmatched entries */
    for (size_t i = 0; i < target.idx->nr; ++i) {
        struct ldif_index_entry *ent = &target.idx->ent[i];

        if (!ent->matched) {
            ++added_cnt;
            struct ldif_entry _ent;
            const char *next = ent->ptr;
            _ent.dn = ent->dn;
            ldif_entry_parse(&target, &next, &_ent);
            ldif_entry_add(&_ent);
            ldif_entry_free(&_ent);
        }
    }

    _log("Finished processing\n");
    _log("Added %lu entries\n", added_cnt);
    _log("Modified %lu entries\n", modified_cnt);
    _log("Deleted %lu entries\n", deleted_cnt);

    /* destroy indexes */
    ldif_index_free(base.idx);
    ldif_index_free(target.idx);
    free(base.idx);
    free(target.idx);

    ldif_close(&base);
    ldif_close(&target);

    return 0;

}

int main(int argc, char **argv)
{
    if (argc != 3) {
        usage();
        exit(-1);
    }

    return diff(argv[1], argv[2]);
}
