#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ldiff/index.h>
#include <ldiff/ldif.h>

static int compar(const void *_a, const void *_b)
{
    const struct ldif_index_entry *a = (const struct ldif_index_entry *) _a;
    const struct ldif_index_entry *b = (const struct ldif_index_entry *) _b;

    return strcasecmp(a->dn, b->dn);
}

static int ldif_index_sort(struct ldif_index *index)
{
    qsort(index->ent, index->nr, sizeof(struct ldif_index_entry), compar);
    return 0;
}

int ldif_index_build(struct ldif *ldif, struct ldif_index *index)
{
    size_t nr = 0;

    /* find "dn:" */
    const char *ptr = ldif->map;

    while ((ptr = memmem(ptr, ldif->size - (ptr - ldif->map), "\ndn:", 4))) {
        const char *next = ptr + 1;
        struct ldif_attr attr;
        ldif_line_parse(ldif, &next, &attr);

        free((void *) attr.key);

        index->ent[nr].dn  = attr.value;
        index->ent[nr].ptr = next;

        ptr = next;
        ++nr;

        if (nr % 100000 == 0)
            _log("\rfound %lu entries", nr);
    }

    _log("\rfound %lu entries\n", nr);
    index->nr = nr;

    return ldif_index_sort(index);
}

void ldif_index_dump(struct ldif_index *index)
{
    for (size_t i = 0; i < index->nr; ++i) {
        fprintf(stderr, "entry %lu: dn=%s\n%.20s\n\n", i,
                index->ent[i].dn, index->ent[i].ptr);
    }
}

struct ldif_index_entry *ldif_index_lookup(struct ldif_index *index, const char *dn)
{
    /* Perform binary search on entries */
    struct ldif_index_entry entry = {.dn = dn, .ptr = NULL};
    return bsearch(&entry, index->ent, index->nr,
            sizeof(struct ldif_index_entry), compar);
}

void ldif_index_free(struct ldif_index *index)
{
    for (size_t i = 0; i < index->nr; ++i) {
        free((void *) index->ent[i].dn);
    }
}
